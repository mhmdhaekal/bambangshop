# BambangShop Publisher App

Tutorial and Example for Advanced Programming 2024 - Faculty of Computer Science, Universitas Indonesia

---

## About this Project

In this repository, we have provided you a REST (REpresentational State Transfer) API project using Rocket web framework.

This project consists of four modules:

1.  `controller`: this module contains handler functions used to receive request and send responses.
    In Model-View-Controller (MVC) pattern, this is the Controller part.
2.  `model`: this module contains structs that serve as data containers.
    In MVC pattern, this is the Model part.
3.  `service`: this module contains structs with business logic methods.
    In MVC pattern, this is also the Model part.
4.  `repository`: this module contains structs that serve as databases and methods to access the databases.
    You can use methods of the struct to get list of objects, or operating an object (create, read, update, delete).

This repository provides a basic functionality that makes BambangShop work: ability to create, read, and delete `Product`s.
This repository already contains a functioning `Product` model, repository, service, and controllers that you can try right away.

As this is an Observer Design Pattern tutorial repository, you need to implement another feature: `Notification`.
This feature will notify creation, promotion, and deletion of a product, to external subscribers that are interested of a certain product type.
The subscribers are another Rocket instances, so the notification will be sent using HTTP POST request to each subscriber's `receive notification` address.

## API Documentations

You can download the Postman Collection JSON here: https://ristek.link/AdvProgWeek7Postman

After you download the Postman Collection, you can try the endpoints inside "BambangShop Publisher" folder.
This Postman collection also contains endpoints that you need to implement later on (the `Notification` feature).

Postman is an installable client that you can use to test web endpoints using HTTP request.
You can also make automated functional testing scripts for REST API projects using this client.
You can install Postman via this website: https://www.postman.com/downloads/

## How to Run in Development Environment

1.  Set up environment variables first by creating `.env` file.
    Here is the example of `.env` file:
    ```bash
    APP_INSTANCE_ROOT_URL="http://localhost:8000"
    ```
    Here are the details of each environment variable:
    | variable | type | description |
    |-----------------------|--------|------------------------------------------------------------|
    | APP_INSTANCE_ROOT_URL | string | URL address where this publisher instance can be accessed. |
2.  Use `cargo run` to run this app.
    (You might want to use `cargo check` if you only need to verify your work without running the app.)

## Mandatory Checklists (Publisher)

- [x] Clone https://gitlab.com/ichlaffterlalu/bambangshop to a new repository.
- **STAGE 1: Implement models and repositories**
  - [x] Commit: `Create Subscriber model struct.`
  - [x] Commit: `Create Notification model struct.`
  - [x] Commit: `Create Subscriber database and Subscriber repository struct skeleton.`
  - [x] Commit: `Implement add function in Subscriber repository.`
  - [x] Commit: `Implement list_all function in Subscriber repository.`
  - [x] Commit: `Implement delete function in Subscriber repository.`
  - [x] Write answers of your learning module's "Reflection Publisher-1" questions in this README.
- **STAGE 2: Implement services and controllers**
  - [x] Commit: `Create Notification service struct skeleton.`
  - [x] Commit: `Implement subscribe function in Notification service.`
  - [x] Commit: `Implement subscribe function in Notification controller.`
  - [x] Commit: `Implement unsubscribe function in Notification service.`
  - [x] Commit: `Implement unsubscribe function in Notification controller.`
  - [x] Write answers of your learning module's "Reflection Publisher-2" questions in this README.
- **STAGE 3: Implement notification mechanism**
  - [x] Commit: `Implement update method in Subscriber model to send notification HTTP requests.`
  - [x] Commit: `Implement notify function in Notification service to notify each Subscriber.`
  - [x] Commit: `Implement publish function in Program service and Program controller.`
  - [x] Commit: `Edit Product service methods to call notify after create/delete.`
  - [x] Write answers of your learning module's "Reflection Publisher-3" questions in this README.

## Your Reflections

This is the place for you to write reflections:

### Mandatory (Publisher) Reflections

#### Reflection Publisher-1

**Number 1**
In the Observer pattern diagram explained by the Head First Design Pattern book, Subscriber is defined as an interface. Explain based on your understanding of Observer design patterns, do we still need an interface (or trait in Rust) in this BambangShop case, or a single Model struct is enough?

**Answer**

Based on the problem i know there is three tpye of notification:

- There is a new Product with the same product type, or
- A Product with the same product type is deleted from the store, or
- BambangShop promotes a Product with the same product type.

in my understanding of observer design pattern, instead of struct called observe it use "subscriber" interface to declare a contract and method without any implementation details. So if there is a multiple observer, we can extend the subscriber and create own implementation. So i think we still need interface (trait) in BambangShop problems, to achieve:

- multiple observer types
- extensibility
- loose coupling
- code organization

**Number 2**

_id in Program and url in Subscriber is intended to be unique. Explain based on your understanding, is using Vec (list) sufficient or using DashMap (map/dictionary) like we currently use is necessary for this case?_

**Answer**

Based on my understanding, using DashMap is necessary for this case in order to achieve efficient. If we using Vec to implement uniquness in id in Product and url in Subscriber, we have to iterate and check id or url for every items in Vec to make sure uniqueness id or url. So i think DashMap is a best case to implment unique id or url, because a single HashMap instance cannot have multiple key.

**Number 3**

_When programming using Rust, we are enforced by rigorous compiler constraints to make a thread-safe program. In the case of the List of Subscribers (SUBSCRIBERS) static variable, we used the DashMap external library for thread safe HashMap. Explain based on your understanding of design patterns, do we still need DashMap or we can implement Singleton pattern instead?_

**Answer**

In the context of web application we implement and use concurency to handle request. We expect the web application will use multiple thread while run in concurenncy. In this case we use list of subscribser (SUBSCRIBER) as static variable that used DashMap package which a package that handled concurrency for sharable and mutable HashMap. DashMap provides more efficient and safer operations for conccurent reads and writes.

Based on my understanding of design pattern, singleton pattern is make sure an object have only one instance throughout the application's lifetime. Which in the case of the List of Subscriber, when implement singleton pattern, the HashMap only one instance and can be access from global. There is a problem when using a HashMap singleton while running the app in multi thread, the problem is i have to handle the lock and unlick mutex when i want to access the HashMap.

In summary, while i can implement HashMap with singleton pattern with Mutex for thread-safety while running apps in multi thread, i think using DashMap is better choice for the case of web application, DashMap already handle concurrency for sharable and mutable hashmap that already provide thread-safety guarantess.

#### Reflection Publisher-2

**Number 1**

_In the Model-View Controller (MVC) compound pattern, there is no “Service” and “Repository”. Model in MVC covers both data storage and business logic. Explain based on your understanding of design principles, why we need to separate “Service” and “Repository” from a Model?_

**Answer**

Based on my understanding seperation of concern is very important, that's main reason why seperate "Service" and "Repository" rather than Model. Another reason to seperate reason
"Service" and "Repository" is for decoupling, testability, scalibity and flexibility.


**Number 2**

_What happens if we only use the Model? Explain your imagination on how the
interactions between each model (Program, Subscriber, Notification) affect the
code complexity for each model?_

If we only use Model only rather than seperated "Service" and "Repository" all
business logic will be stored in model which increase code complexity in all mo
dules. Each model will handle all of data structure, properties of programs, and
business logic. If the modules need to communicate with other modules, it also
intreacting in Model which increase code complexity and harder debugging.

**Number 3**

_Have you explored more about Postman? Tell us how this tool helps you to test your current work. You might want to also list which features in Postman you are interested in or feel like it is helpful to help your Group Project or any of your future software engineering projects._

In this projects im not using postman for api client in my local computer, im using [bruno api](https://www.usebruno.com),here's why im not using postman:

- Lack of git collaboration in free tier
- collaboration need paid tier
- Limited features in free tier

But if i have to list which features in postman im interested, this is list of
it:

- Sharable collection
- Chain request and automation for testing

#### Reflection Publisher-3

**Number 1**

_Observer Pattern has two variations: Push model (publisher pushes data to subscribers) and Pull model (subscribers pull data from publisher). In this tutorial case, which variation of Observer Pattern that we use?_

**Answer**

Push Model, as we can see in log

```bash
POST /notification/subscribe/APPLIANCES application/json:
   >> Matched: (subscribe) POST /notification/subscribe/<product_type>
   >> Outcome: Success(201 Created)
   >> Response succeeded.
GET /product/ application/json:
   >> Matched: (list) GET /product/
   >> Outcome: Success(200 OK)
   >> Response succeeded.
POST /product/ application/json:
   >> Matched: (create) POST /product/
   >> Outcome: Success(201 Created)
   >> Response succeeded.
   >> Send CREATED notification of: [APPLIANCES] Sapu Cap Bambang, to: https://www.youtube.com/watch?v=dQw4w9WgXcQ

```

The publisher send notification to subscriber.


**Number 2**

_What are the advantages and disadvantages of using the other variation of Observer Pattern for this tutorial case? (example: if you answer Q1 with Push, then imagine if we used Pull)_

advantages:

- reduced coupling
- Better control of subscriber, the publisher doesn't have to stored list of subcriber,

disadvantages:

- potential missing updates, because subscriber need to check for notification,
- increased complexity in subscriber or in this context receiver apps,
- Inefficient for Real-time Updates

**Number 3**

_Explain what will happen to the program if we decide to not use multi-threading in the notification process._

If not use multi-threading it will use only thread and causing several problems:

- Blocking main thread, the main thread will wait on notification sending which not efficient.
- Increased response times.
- Queueing of request,because of main thread blocked by notification, other request have to wait until the notification sending finised.
- Timeout, because of queieng of request it will lead to request timeout for long response from main application.
